﻿using Zenject;
using UnityEngine;
using System;

public class FieldInstaller : MonoInstaller
{

    //game setting
    public GameObject gameManagerPrefab;
    public UIHelperRect infoPanel;

    //cell setting
    public GameObject cellPrefab;
    public GameObject ingotPrefab;


    public override void InstallBindings()
    {
        GameSetup();
        SetupFieldGenerator();
        SetupCellsGenerator();
        InstallSignals();
        InstallGameMisc();
    }

    private void InstallSignals()
    {
        SignalBusInstaller.Install(Container);
        Container.DeclareSignal<SaveGameSignal>();
        Container.DeclareSignal<LoadGameSignal>();
        Container.DeclareSignal<CellDugSignal>();
        Container.DeclareSignal<RestartButtonPressedSignal>();
        Container.DeclareSignal<ExitButtonPressedSignal>();
        Container.DeclareSignal<GameFinishSignal>();
        Container.DeclareSignal<GameLoadedSignal>();
        Container.DeclareSignal<StatsLoadedSignal>();
        Container.DeclareSignal<RestartGameSignal>();
        Container.DeclareSignal<SpendShovelSignal>();
        Container.DeclareSignal<IngotCountChangeSignal>();
        Container.DeclareSignal<DropIngotSignal>();
        Container.DeclareSignal<CollectIngotSignal>();
        Container.DeclareSignal<BlockButtonsSignal>();
        Container.DeclareSignal<UnblockButtonsSignal>();
        Container.DeclareSignal<CountIngotSignal>();


    }

    private void InstallGameMisc()
    {
        Container.BindInterfacesAndSelfTo<GameSavesSystem>().AsSingle();
        Container.BindInterfacesAndSelfTo<GameStats>().AsSingle();
    }

    private void SetupCellsGenerator()
    {
        Container.BindFactory<CellGameLogic.CellSettings, ICellController, CellController.CellControllerFactory>()
            .To<CellController>()
            .AsSingle()
            .WhenInjectedInto<CellsGenerator>();
        Container.BindFactory<CellGameLogic.CellSettings,ICellModel, CellController.CellModelFactory>()
            .To<CellModel>()
            .AsCached()
            .WhenInjectedInto<CellController>();
        Container.BindFactory<ICellView, CellController.CellViewFactory>()
            .To<CellView>()
            .FromComponentInNewPrefab(cellPrefab)
            .UnderTransformGroup("Cells")
            .AsCached()
            .WhenInjectedInto<CellController>();
        Container.BindFactory<ICellGameLogic, CellController.CellGameLogicFactory>()
            .To<CellGameLogic>()
            .AsCached()
            .WhenInjectedInto<CellController>();
        Container.BindFactory<IReward, IngotGenerator.IngotFactory>()
            .To<IngotView>()
            .FromComponentInNewPrefab(ingotPrefab)
            .AsCached();
        Container.Bind<IngotGenerator>().AsSingle();

    }

    

    private void SetupFieldGenerator()
    {
        Container.Bind<IFieldGenerator>()
            .To<FieldGenerator>()
            .AsSingle();
        Container.BindFactory<FieldModel.FieldModelSettings, IFieldModel, FieldGenerator.FieldModelFactory>()
            .To<FieldModel>()
            .AsCached();
        Container.BindFactory< IFieldModel, ICellsGenerator, FieldGenerator.CellGeneratorFactory>()
             .To<CellsGenerator>()
             .AsSingle();
        Container.BindFactory<IFieldView, FieldGenerator.FieldViewFactory>()
            .To<FieldView>()
            .FromNewComponentOnNewGameObject();
    }

    

    private void GameSetup()
    {
        Container.BindInstance(infoPanel);
        Container.BindInterfacesAndSelfTo<GameManagerCreator>()
            .AsSingle();
        Container.BindFactory<FieldGenerator.Settings, GameManager, GameManager.GameManagerFactory>()
            .FromComponentInNewPrefab(gameManagerPrefab)
            .AsSingle();

        Container.Bind<IUIHelperRect>()
            .To<UIHelperRect>()
            .FromComponentOn(infoPanel.gameObject)
            .AsTransient();
    }

    

}



