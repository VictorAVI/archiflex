﻿using UnityEngine;

internal interface IUIHelperRect
{
    RectTransform RectTransform { get; }
    Transform Transform { get; }
}






