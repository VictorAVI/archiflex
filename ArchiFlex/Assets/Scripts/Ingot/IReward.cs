﻿using UnityEngine;

public interface IReward
{
    void SetPosition(Vector3 position);
    GameObject GameObject { get; }
}

