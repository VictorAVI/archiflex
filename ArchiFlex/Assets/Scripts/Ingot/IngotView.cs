﻿using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class IngotView:MonoBehaviour,IReward, IDragHandler, IBeginDragHandler, IEndDragHandler
{
    private float z;
    private Vector3 startPos;

    public GameObject GameObject => gameObject;

    [Inject]
    public void Construct()
    {

    }

    

    public void OnBeginDrag(PointerEventData eventData)
    {
        z = transform.position.z;
        startPos = transform.position;
        GetComponent<BoxCollider2D>().enabled = false;
        GetComponent<SpriteRenderer>().sortingOrder = 1;
    }

    public void OnDrag(PointerEventData eventData)
    {
        var newPos = Camera.main.ScreenToWorldPoint(eventData.position);
        newPos.z = z;
        transform.position = newPos;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.position = startPos;
        GetComponent<BoxCollider2D>().enabled = true;
        GetComponent<SpriteRenderer>().sortingOrder = 0;
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
    }
}

