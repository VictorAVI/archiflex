using UnityEngine;
using Zenject;
using System.Collections.Generic;

public partial class IngotGenerator
{
    CellGameLogic.Settings logicSettings;
    IngotFactory ingotFactory;

    Dictionary<IReward,ICellModel> rewardsLink = new Dictionary<IReward,ICellModel >();
    SignalBus signalBus;
    private int currentIngotSpawns = 0;
    public IngotGenerator(CellGameLogic.Settings logicSettings, IngotFactory ingotFactory, SignalBus signalBus)
    {
        this.logicSettings = logicSettings;
        this.ingotFactory = ingotFactory;
        this.signalBus = signalBus;
        this.signalBus.Subscribe<RestartGameSignal>(OnRestartGame);
        this.signalBus.Subscribe<DropIngotSignal>(OnIngotDrop);
    }

    private void OnRestartGame()
    {
        foreach (var item in rewardsLink)
        {
            GameObject.Destroy(item.Key.GameObject);
        }
        rewardsLink.Clear();
        currentIngotSpawns = 0;
        signalBus.Fire(new IngotCountChangeSignal() { currentIngotsCount = currentIngotSpawns });
    }

    private void OnIngotDrop(DropIngotSignal args)
    {
        var reward = args.ingot.GetComponent<IReward>();
        if(rewardsLink.ContainsKey(reward))
        {
            rewardsLink[reward].BlockedBy = false;
            rewardsLink[reward].HasVisibleReward = false;
            rewardsLink[reward].Interactable = true;
            rewardsLink.Remove(reward);
        }
        GameObject.Destroy(reward.GameObject);
        currentIngotSpawns--;
        signalBus.Fire(new IngotCountChangeSignal() { currentIngotsCount = currentIngotSpawns });
        signalBus.Fire<CountIngotSignal>();

    }

    internal bool GenerateIngot(ICellModel cellModel, ICellView cellView)
    {
        var val = Random.Range(0, 100);
        if (val <= logicSettings.chanceToGenerateIngot)
        {
            GetIngot( cellModel,  cellView);
            currentIngotSpawns++;
            signalBus.Fire(new IngotCountChangeSignal() { currentIngotsCount = currentIngotSpawns });
            return true;
        }       
        return false;
    }

    internal void PutIngot(ICellModel cellModel, ICellView cellView)
    {
         GetIngot(cellModel,cellView);
    }

    private IReward GetIngot(ICellModel cellModel, ICellView cellView)
    {
        var ingot = ingotFactory.Create();
        ingot.SetPosition(cellView.Transform.position);
        rewardsLink.Add(ingot, cellModel);
        return ingot;

    }

    public class IngotFactory : PlaceholderFactory<IReward> { }
    
}

