using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public class DropzoneTarget : MonoBehaviour, IDropHandler
{
    SignalBus signalBus;

    [Inject]
    public void Construct(SignalBus signalBus)
    {
        this.signalBus = signalBus;
    }

    public void OnDrop(PointerEventData eventData)
    {
        signalBus.Fire(new DropIngotSignal() {ingot = eventData.pointerDrag });

    }
}
