﻿public class UIShovelsView:UICanvasStatView
{
    public void Start()
    {
        signalBus.Subscribe<SpendShovelSignal>(OnSpendShovel);
    }

    private void OnSpendShovel(SpendShovelSignal args)
    {
        count.text = args.shovelsCount.ToString();
    }

    protected override void OnStatsLoaded(StatsLoadedSignal args)
    {
        count.text = args.gameStats.ShovelsCount.ToString();
    }
}
