﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public abstract class UICanvasStatView: MonoBehaviour
{
    [SerializeField] protected Text count;
    protected SignalBus signalBus;

    [Inject]
    public void Construct(SignalBus signalBus)
    {
        this.signalBus = signalBus;
        signalBus.Subscribe<StatsLoadedSignal>(OnStatsLoaded);

    }

    protected abstract void OnStatsLoaded(StatsLoadedSignal args);




}
