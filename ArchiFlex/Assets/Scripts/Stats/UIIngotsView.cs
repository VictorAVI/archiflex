﻿using UnityEngine;
using UnityEngine.UI;

public class UIIngotsView : UICanvasStatView
{
    [SerializeField] private Text demand;

    public void Start()
    {
        signalBus.Subscribe<CollectIngotSignal>(OnCollectIngot);

    }

    private void OnCollectIngot(CollectIngotSignal args)
    {
        count.text = args.ingotCount.ToString();
    }

    protected override  void OnStatsLoaded(StatsLoadedSignal args)
    {
        count.text = args.gameStats.IngotCount.ToString();
        demand.text = args.gameStats.IngotsToWin.ToString();
    }

}
