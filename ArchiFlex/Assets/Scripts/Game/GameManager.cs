using System.Collections;
using UnityEngine;
using Zenject;

public class GameManager : MonoBehaviour
{
    private IUIHelperRect infoPanel;
    private GameEndPanel gameEndPanel;
    private IFieldGenerator fieldController;
    private SignalBus signalBus;

    FieldGenerator.Settings fieldSettings;

    private void Start()
    {        
        StartCoroutine( StartGame());
    }
    
   
    private IEnumerator StartGame()
    {
        yield return new WaitForEndOfFrame();
        
        signalBus.Fire<LoadGameSignal>();

        
    }

    private FieldModel.FieldModelSettings CalculateScreen(FieldGenerator.Settings settings)
    {
        var canvas = infoPanel.RectTransform.gameObject.GetComponentInParent<Canvas>();
        var width = canvas.pixelRect.width;
        var avialableWidth = width - infoPanel.RectTransform.rect.width*canvas.scaleFactor;
        var avialableHeight = infoPanel.RectTransform.rect.height * canvas.scaleFactor;

        var avialableCellWidth = avialableWidth / settings.columnsCount;// - settings.space;
        var avialableCellHeight = avialableHeight / settings.rowsCount;// - settings.space;
        var usableCellSize = 0f;
        if(avialableCellHeight<avialableCellWidth)
        {
            usableCellSize = avialableCellHeight;
        }
        else
        {
            usableCellSize = avialableCellWidth;
        }

        Debug.Log("Avalable width "+ avialableWidth);
        Debug.Log("Avalable cell width "+ avialableCellWidth);

        var cellRect = new Rect(settings.space, settings.space, usableCellSize, usableCellSize);
        var fieldModelSettings = new FieldModel.FieldModelSettings()
        {
            aviableFieldRect = new Rect(0, 0, avialableWidth, avialableHeight),
            cellRect = cellRect


        };
        return fieldModelSettings;
    }

    [Inject]
    private void Construct( IFieldGenerator fieldController, IUIHelperRect infoPanel, SignalBus signalBus, GameEndPanel gameEndPanel, FieldGenerator.Settings fieldSettings)
    {
        this.fieldController = fieldController;
        this.infoPanel = infoPanel;
        this.signalBus = signalBus;
        this.gameEndPanel = gameEndPanel;
        this.fieldSettings = fieldSettings;
        signalBus.Subscribe<RestartButtonPressedSignal>(RestartGame);
        signalBus.Subscribe<GameFinishSignal>(GameFinish);
        signalBus.Subscribe<GameLoadedSignal>(GameLoaded);
    }

    void RestartGame()
    {
        gameEndPanel.gameObject.SetActive(false);
        fieldController.RegenerateField();
        signalBus.Fire<RestartGameSignal>();
    }

    void GameFinish(GameFinishSignal args)
    {
        gameEndPanel.gameObject.SetActive(true);
        
    }

    void GameLoaded(GameLoadedSignal args)
    {
        var fieldModelSettings = CalculateScreen(fieldSettings);
        if (args.gameSaveData.newGame)
        {
            fieldController.GenerateField(fieldModelSettings);
        }
        else
        {
            fieldController.LoadFieldFromSavedData(args.gameSaveData, fieldModelSettings);
        }
    }


    public class GameManagerFactory : PlaceholderFactory<FieldGenerator.Settings, GameManager> { }
   

}





