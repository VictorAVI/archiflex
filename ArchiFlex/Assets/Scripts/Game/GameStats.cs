﻿using UnityEngine;
using System.Runtime.Serialization;
using Zenject;
using System;

[DataContract]
public class GameStats:IInitializable, IDisposable
{
    CellGameLogic.Settings logicSettings;

    [DataMember]
    private int shovelsCount;
    [DataMember]
    private int ingotCount;

    internal int ShovelsCount { get => shovelsCount; }
    internal int IngotCount { get => ingotCount; }
    internal int IngotsToWin { get; set; }

    private int currentIngotCount;

    readonly SignalBus signalBus;
    public GameStats(SignalBus signalBus, CellGameLogic.Settings settings)
    {
        this.signalBus = signalBus;
        logicSettings = settings;
        InitDefault();
    }

    private void InitDefault()
    {
        shovelsCount = logicSettings.startShovelsCount;
        ingotCount = 0;
        IngotsToWin = logicSettings.ingotsToWin;
        
    }

    private void OnCellDug()
    {
        if (shovelsCount > 0)
        {
            shovelsCount--;
            signalBus.Fire(new SpendShovelSignal() { shovelsCount = shovelsCount });
            if (shovelsCount <= 0 && currentIngotCount <= 0)
                signalBus.Fire(new GameFinishSignal() { gameFinishType = GameFinishType.LOST });

        }

    }

    public void Dispose()
    {
        signalBus.Unsubscribe<CellDugSignal>(OnCellDug);
        signalBus.Unsubscribe<GameLoadedSignal>(OnGameLoaded);
        signalBus.Unsubscribe<CountIngotSignal>(OnCountIngot);
        signalBus.Unsubscribe<RestartGameSignal>(OnStatsReset);
        signalBus.Unsubscribe<IngotCountChangeSignal>(OnIngotCountChanged);
    }

    

    public void Initialize()
    {
        signalBus.Subscribe<CellDugSignal>(OnCellDug);
        signalBus.Subscribe<GameLoadedSignal>(OnGameLoaded);
        signalBus.Subscribe<IngotCountChangeSignal>(OnIngotCountChanged);
        signalBus.Subscribe<CountIngotSignal>(OnCountIngot);
        signalBus.Subscribe<RestartGameSignal>(OnStatsReset);

    }

    private void OnStatsReset()
    {
        InitDefault();
        signalBus.Fire(new StatsLoadedSignal() { gameStats = this });
    }

    private void OnGameLoaded(GameLoadedSignal args)
    {
        if (!args.gameSaveData.newGame)
        {
            shovelsCount = args.gameSaveData.gameStats.shovelsCount;
            ingotCount = args.gameSaveData.gameStats.ingotCount;
            args.gameSaveData.gameStats.IngotsToWin = IngotsToWin;
            signalBus.Fire(new StatsLoadedSignal() { gameStats = args.gameSaveData.gameStats });
        }
        else
        {
            InitDefault();
            signalBus.Fire(new StatsLoadedSignal() { gameStats = this });
        }



    }

    private void OnCountIngot()
    {
        ingotCount++;
        signalBus.Fire(new CollectIngotSignal() { ingotCount = ingotCount });
        if (ingotCount >= IngotsToWin)
            signalBus.Fire(new GameFinishSignal() { gameFinishType = GameFinishType.WIN });
        else if (shovelsCount <= 0 && currentIngotCount <= 0)
            signalBus.Fire(new GameFinishSignal() { gameFinishType = GameFinishType.LOST });

    }

    private void OnIngotCountChanged(IngotCountChangeSignal args)
    {
        currentIngotCount = args.currentIngotsCount;

    }



}
