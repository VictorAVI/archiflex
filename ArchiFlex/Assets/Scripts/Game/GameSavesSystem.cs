﻿using System.Runtime.Serialization;
using Zenject;
using UnityEngine;
using System;
using System.IO;

interface IGameSaveSystem
{
}


[DataContract]
public class GameSavesSystem: IGameSaveSystem,IInitializable, IDisposable
{
    const string fileName = "save.eff";

    GameStats gameStats;
    IFieldGenerator fieldController;

    readonly SignalBus signalBus;

    internal GameSavesSystem(SignalBus signalBus, IFieldGenerator fieldController,GameStats gameStats)
    {
        this.signalBus = signalBus;
        this.fieldController = fieldController;
        this.gameStats = gameStats;

    }

    private void SaveGame()
    {
        var dest = Path.Combine(Application.persistentDataPath, fileName);
        GameSaveData gameSave = new GameSaveData();
        gameSave.cellModels = fieldController.CellModels();
        gameSave.gameStats = gameStats;

        MemoryStream stream = new MemoryStream();
        DataContractSerializer serializer = new DataContractSerializer(typeof(GameSaveData));
        serializer.WriteObject(stream, gameSave);



#if NETCORE_FX
            UnityEngine.Windows.File.WriteAllByte(dest, imagePtr);
#else
        File.WriteAllBytes(dest, stream.ToArray());
#endif
        stream.Dispose();
    }


    private void LoadGame()
    {
        GameSaveData gameSave;
        var dest = Path.Combine(Application.persistentDataPath, fileName);
        if (File.Exists((string)dest))
        {
            DataContractSerializer dcs = new DataContractSerializer(typeof(GameSaveData));
            using (Stream stream = File.OpenRead((string)dest))
            {
                gameSave = (GameSaveData)dcs.ReadObject(stream);
                gameSave.newGame = false;
            }
        }
        else
            gameSave = new GameSaveData();
        signalBus.Fire(new GameLoadedSignal() { gameSaveData = gameSave });

    }

    public void Dispose()
    {
        signalBus.Unsubscribe<SaveGameSignal>( SaveGame);
        signalBus.Unsubscribe<LoadGameSignal>(LoadGame);
        signalBus.Unsubscribe<ExitButtonPressedSignal>(ExitGame);



    }

    public void Initialize()
    {
        signalBus.Subscribe<SaveGameSignal>( SaveGame);
        signalBus.Subscribe<LoadGameSignal>(LoadGame);
        signalBus.Subscribe<ExitButtonPressedSignal>(ExitGame);

    }

    private void ExitGame()
    {
        SaveGame();
        if (Application.isEditor)
            UnityEditor.EditorApplication.isPlaying = false;
        else
            Application.Quit();
    }
}



