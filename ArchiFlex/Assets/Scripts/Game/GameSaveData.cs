﻿using System.Runtime.Serialization;
using System.Collections.Generic;

[DataContract]
public class GameSaveData
{
    [DataMember]
    internal List<CellModel> cellModels;

    [DataMember]
    internal GameStats gameStats;

    internal bool newGame = true;
}



