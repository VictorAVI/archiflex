﻿using Zenject;

public class GameManagerCreator:IInitializable
{
    readonly GameManager.GameManagerFactory gameManagerFactory;
    FieldGenerator.Settings fieldSettings;
    public GameManagerCreator(GameManager.GameManagerFactory gameManagerFactory,FieldGenerator.Settings fieldSettings)
    {
        this.gameManagerFactory = gameManagerFactory;
        this.fieldSettings = fieldSettings;
    }

    public void Initialize()
    {
        var gameManager = gameManagerFactory.Create(fieldSettings);
    }
}



