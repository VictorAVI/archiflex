using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="New Cell Depth Color", menuName ="SO/Create Cell Depth Color")]
public class CellColors : ScriptableObject
{
    [SerializeField] private List<Color32> depthColors;


    internal Color InitialColor { get => depthColors[0]; }
    internal int ColorCount { get => depthColors.Count; }
    internal List<Color32> Colors { get => depthColors; }

}
