using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "FieldSOInstaller", menuName = "Installers/FieldSOInstaller")]
public class FieldSOInstaller : ScriptableObjectInstaller<FieldSOInstaller>
{
    public FieldGenerator.Settings fieldSettings;
    public CellGameLogic.Settings logicSettings;
    public CellGameLogic.CellSettings cellSettings;
    

    public override void InstallBindings()
    {
        Container.BindInstance(fieldSettings);
        Container.BindInstance(logicSettings);
        Container.BindInstance(cellSettings);

    }
}