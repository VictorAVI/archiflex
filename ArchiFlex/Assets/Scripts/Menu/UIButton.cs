﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public abstract class UIButton:MonoBehaviour
{
    [SerializeField] protected Button button;
    protected SignalBus signalBus;

    [Inject]
    public void Inject(SignalBus signalBus)
    {
        this.signalBus = signalBus;
        this.signalBus.Subscribe<BlockButtonsSignal>(OnBlockButtons);
        this.signalBus.Subscribe<UnblockButtonsSignal>(OnUnblockButtons);
    }

    private void OnBlockButtons()
    {
        button.interactable = false;
    }

    private void OnUnblockButtons()
    {
        button.interactable = true;

    }
}
