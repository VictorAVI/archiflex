using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class GameEndPanel : MonoBehaviour
{
    [SerializeField] private Text gameEndText;
    [SerializeField] Color winTextColor;
    [SerializeField] Color lostTextColor;

    [Inject]
    public void Construct(SignalBus signalBus)
    {
        signalBus.Subscribe<GameFinishSignal>(OnGameFinish);
    }

    private void OnGameFinish(GameFinishSignal args)
    {
        switch(args.gameFinishType)
        {
            case GameFinishType.LOST:
                GameLost();
                break;
            case GameFinishType.WIN:
                GameWin();
                break;
            default:
                throw new System.NotImplementedException("No such game finish type");
        }
    }

    private void GameWin()
    {
        gameEndText.text = "Game Win";
        gameEndText.color = winTextColor;
    }

    private void GameLost()
    {
        gameEndText.text = "Game Lost";
        gameEndText.color = lostTextColor;
    }
}

