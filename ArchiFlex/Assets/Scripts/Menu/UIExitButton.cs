﻿public class UIExitButton : UIButton
{
    private void Awake()
    {
        button.onClick.AddListener(ExitGame);
    }

    private void ExitGame()
    {
        signalBus.Fire<ExitButtonPressedSignal>();
    }

}
