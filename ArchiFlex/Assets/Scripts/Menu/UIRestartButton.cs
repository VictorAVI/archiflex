public class UIRestartButton : UIButton
{
    private void Awake()
    {
        button.onClick.AddListener(RestartGame);
    }

    void RestartGame()
    {
        signalBus.Fire<RestartButtonPressedSignal>();
    }

}
