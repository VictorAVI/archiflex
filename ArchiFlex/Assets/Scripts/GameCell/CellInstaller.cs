﻿using Zenject;
using UnityEngine;
using System;

public class CellInstaller : Installer
{
    public GameObject prefab;
    public override void InstallBindings()
    {

        /*BindCellModel();
        BindCellView();
        BindCellController();*/
    }

    private void BindCellController()
    {
        var cellController = Container.Instantiate<CellController>();
        Container.Bind<ICellView>().AsSingle();
        Container.Bind<ICellModel>().AsSingle();
    }

    private void BindCellView()
    {
        var cellView = Container.InstantiatePrefabForComponent<ICellView>(prefab);
        Container
            .Bind<ICellView>()
            .FromInstance(cellView)
            .AsSingle();
    }

    private void BindCellModel()
    {
        Container
            .Bind<ICellModel>()
            .AsSingle();
    }

}