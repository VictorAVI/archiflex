using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

public interface IView
{
    void PlaceView(Vector3 position);
    GameObject GameObject { get; }


}

public delegate void ViewClick();

public  interface ICellView:IView, IPointerClickHandler
{
    event ViewClick OnViewClick;
    void SetCellColor(Color color);
    Transform Transform { get; }
}

public class CellView : MonoBehaviour, ICellView
{
    public event ViewClick OnViewClick;

    private Color cellColor = Color.white;
    private SpriteRenderer cellSpriteRenerer;

    public GameObject GameObject => gameObject;

    public Transform Transform => transform;

    public void OnPointerClick(PointerEventData eventData)
    {
        OnViewClick?.Invoke();
    }

    [Inject]
    public void Construct()
    {
        
    }

    void Awake()
    {
        cellSpriteRenerer = GetComponent<SpriteRenderer>();
    }

    public void PlaceView(Vector3 position)
    {
        transform.position = position;
    }

    public void SetCellColor(Color color)
    {
        cellSpriteRenerer.color = color;
    }
}

struct TransformComponent
{
    public Transform Transform;
}

struct CellViewComponent 
{
    public ICellView cellView;
}



class CellDrawSystem
{
    struct Component
    {
        TransformComponent rect;
        CellViewComponent cell;
        
    }

 
}