﻿using System;
using UnityEngine;
using Zenject;
using System.Collections.Generic;

interface ICellGameLogic
{
    void CheckLogic(ICellModel cellModel,ICellView cellView);
}


public class CellGameLogic:ICellGameLogic
{
    readonly CellSettings cellSettings;
    readonly SignalBus signalBus;
    readonly IngotGenerator ingotGenerator;
    readonly GameStats gameStats;
    public CellGameLogic(SignalBus signalBus, CellGameLogic.CellSettings cellSettings,IngotGenerator ingotGenerator,GameStats gameStats)
    {
        this.cellSettings = cellSettings;
        this.signalBus = signalBus;
        this.ingotGenerator = ingotGenerator;
        this.gameStats = gameStats;
    }


    public void CheckLogic(ICellModel cellModel, ICellView cellView)
    {
        if (cellModel.DigDeep > 0 && !cellModel.BlockedBy && gameStats.ShovelsCount>0)
        {
            cellModel.DigDeep--;

            var newColor = Color.black;
            if (cellModel.DigDeep != 0)
            {
                newColor = cellSettings.cellColors.Colors[cellSettings.cellColors.ColorCount - cellModel.DigDeep];
            }
            cellView.SetCellColor(newColor);
            var gotIngot = ingotGenerator.GenerateIngot(cellModel, cellView);


            signalBus.Fire<CellDugSignal>();
            if (cellModel.DigDeep <= 0 || gotIngot)
            {
                cellModel.Interactable = false;
                if (gotIngot)
                {
                    cellModel.HasVisibleReward = true;
                    cellModel.BlockedBy = true;
                }
            }

        }
        else
        {
            Debug.Log("DigDeep <= 0; BlockedBy; Shovels=0 ");
        }

    }

    [Serializable]
    public class Settings
    {
        [Range(1, 20)]
        public int startShovelsCount = 20;

        [Range(0, 100)]
        public float chanceToGenerateIngot = 23;

        [Range(0, 50)]
        public int ingotsToWin = 3;
        
    }

    [Serializable]
    public class CellSettings
    {
        [Header("Cell depth colors")]
        public CellColors cellColors;
    }

}





