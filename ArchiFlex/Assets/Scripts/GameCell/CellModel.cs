using System.Runtime.Serialization;
using Zenject;


public interface ICellModel
{
    bool Interactable { get; set; }
    bool BlockedBy { get; set; }
    int DigDeep { get; set; }
    bool HasVisibleReward { get; set; }
}


[DataContract]
public class CellModel : ICellModel
{
    [DataMember]
    public bool Interactable { get; set; } = true;
    [DataMember]
    public bool BlockedBy { get; set; } = false;
    [DataMember]
    public int DigDeep { get; set; }
    [DataMember]
    public bool HasVisibleReward { get; set; } = false;

    public CellModel(CellGameLogic.CellSettings cellSettings)
    {
        DigDeep = cellSettings.cellColors.ColorCount;
    }
}
