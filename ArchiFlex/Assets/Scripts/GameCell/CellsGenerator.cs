﻿using System.Collections.Generic;
using Zenject;
using UnityEngine;
using System.Threading.Tasks;
using System.Threading;


internal interface ICellsGenerator
{
    void GenerateNewCells();
    void Regenerate();
    void LoadCellsFromSave(GameSaveData gameSaveData);
    List<CellModel> GetCellModels();
}

public class CellsGenerator: ICellsGenerator 
{
    internal List<ICellController> cells;
    private IFieldModel fieldModel;
    CellController.CellControllerFactory cellFactory;
    CellGameLogic.CellSettings cellSettings;
    IngotGenerator ingotGenerator;
    SignalBus signalBus;

    private CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

    public CellsGenerator(IFieldModel fieldModel, CellController.CellControllerFactory cellFactory, CellGameLogic.CellSettings cellSettings, IngotGenerator ingotGenerator,SignalBus signalBus)
    {
        this.cellFactory = cellFactory;
        this.fieldModel = fieldModel;
        this.cellSettings = cellSettings;
        this.ingotGenerator = ingotGenerator;
        this.signalBus = signalBus;
    }


    public void GenerateNewCells()
    {
        cells = new List<ICellController>(fieldModel.GridSize);
        signalBus.Fire<BlockButtonsSignal>();
        SpawnCells();
    }

    private async void SpawnCells()
    {
        UnityEditor.EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;
        try
        {
            await SpawnCell(cancellationTokenSource.Token);
        }
        catch { }
        {
        }
        UnityEditor.EditorApplication.playModeStateChanged -= EditorApplication_playModeStateChanged;
        signalBus.Fire<UnblockButtonsSignal>();

    }

    private void EditorApplication_playModeStateChanged(UnityEditor.PlayModeStateChange obj)
    {
        if(obj == UnityEditor.PlayModeStateChange.ExitingPlayMode)
        {
            cancellationTokenSource.Cancel();
        }
    }

    async Task SpawnCell(CancellationToken cancellationToken)
    {
        for (int i = 0; i < fieldModel.GridSize; i++)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                cancellationToken.ThrowIfCancellationRequested();
                return;
            }
            var cellController = cellFactory.Create(cellSettings);
            cellController.InitNew();
            var newCellViewPos = CalculateCellPosition2(i,fieldModel.GridColumns);
            cellController.PlaceView(newCellViewPos);
            cells.Add(cellController);
            await Task.Yield();

        }

    }

    private Vector3 CalculateCellPosition(int cellIndex, int columnIndex)
    {
        var screenXPos = fieldModel.CellRect.x * (columnIndex + 1) + fieldModel.CellRect.width * (columnIndex + 1);
        var screenYPos = fieldModel.CellRect.y * (cellIndex + 1) + fieldModel.CellRect.height * (cellIndex + 1);

        return Camera.main.ScreenToWorldPoint(new Vector3(screenXPos, screenYPos, 11));
    }

    private Vector3 CalculateCellPosition2(int cellIndex, int columnCount)
    {
        var newCellPosition = new Vector3(
            fieldModel.CellRect.width/2 + fieldModel.CellRect.width * (cellIndex % columnCount),
            fieldModel.CellRect.height/2 + fieldModel.CellRect.height * (cellIndex / columnCount),
            11
            );
        return Camera.main.ScreenToWorldPoint(newCellPosition);
    }

    public List<CellModel> GetCellModels()
    {
        var cellModels = new List<CellModel>();
        foreach (var item in cells)
        {
            cellModels.Add(item.CellModel as CellModel);
        }
        return cellModels;
    }

    public void Regenerate()
    {
        for (int i = 0; i < cells.Count; i++)
        {
            cells[i].DestroyView();
        }
        cells.Clear();
        GenerateNewCells();
    
    }

    public void LoadCellsFromSave(GameSaveData gameSaveData)
    {
        signalBus.Fire<BlockButtonsSignal>();
        LoadCells(gameSaveData);
    }

    private async void LoadCells(GameSaveData gameSaveData)
    {
        UnityEditor.EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;

        try
        {
            await LoadCellsTask(gameSaveData, cancellationTokenSource.Token);
        }
        catch { }
        UnityEditor.EditorApplication.playModeStateChanged -= EditorApplication_playModeStateChanged;
        signalBus.Fire<UnblockButtonsSignal>();

    }

    async Task LoadCellsTask(GameSaveData gameSaveData,CancellationToken cancellationToken)
    {
        cells = new List<ICellController>(fieldModel.GridSize);
        var countIndex = 0;
        for (int i = 0; i < fieldModel.GridSize; i++)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                cancellationToken.ThrowIfCancellationRequested();
                return;
            }
            var cellController = cellFactory.Create(cellSettings);
            cellController.LoadFromSave(gameSaveData.cellModels[countIndex]);
            var newCellViewPos = CalculateCellPosition2(i, fieldModel.GridColumns);
            cellController.PlaceView(newCellViewPos);
            if (gameSaveData.cellModels[countIndex].HasVisibleReward)
                ingotGenerator.PutIngot(cellController.CellModel, cellController.CellView);
            countIndex++;
            cells.Add(cellController);
            await Task.Yield();
        }

    }

}



