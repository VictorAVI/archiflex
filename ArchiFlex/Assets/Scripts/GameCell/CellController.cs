using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;

public interface IController
{
    
}

public interface ICellController:IController
{
    void PlaceView(Vector3 position);
    ICellModel CellModel { get; }
    ICellView CellView { get; }

    void DestroyView();
    void InitNew();
    void LoadFromSave(CellModel cellModel);
}

public partial class CellController :ICellController,IDisposable
{
    private ICellView cellView;
    private ICellModel cellModel;
    private ICellGameLogic cellGameLogic;

    CellModelFactory cellModelFactory;
    CellViewFactory cellViewFactory;
    CellGameLogicFactory cellGameLogicFactory;

    readonly CellGameLogic.CellSettings cellSettings;

    ICellView ICellController.CellView  => cellView;

    ICellModel ICellController.CellModel => cellModel;

    internal CellController(CellViewFactory cellViewFactory, CellModelFactory cellModelFactory,CellGameLogicFactory cellGameLogicFactory, CellGameLogic.CellSettings cellSettings)
    {
        this.cellViewFactory = cellViewFactory;
        this.cellModelFactory = cellModelFactory;
        this.cellGameLogicFactory = cellGameLogicFactory;
        
        cellGameLogic = cellGameLogicFactory.Create();
        this.cellSettings = cellSettings;

        
    }

    public void DestroyView()
    {        
        cellView.OnViewClick -= CellView_OnViewClick;
        UnityEngine.GameObject.Destroy(cellView.GameObject);
    }

    public void Dispose()
    {
        cellView.OnViewClick -= CellView_OnViewClick;
    }


    public void InitNew()
    {
        cellModel = cellModelFactory.Create(cellSettings);
        cellView = cellViewFactory.Create();
        cellView.SetCellColor(cellSettings.cellColors.InitialColor);
        cellView.OnViewClick += CellView_OnViewClick;

    }

    public void LoadFromSave(CellModel cellModel)
    {
        this.cellModel = cellModel;
        cellView = cellViewFactory.Create();

        var newColor = Color.black;
        if (cellModel.DigDeep != 0)
        {
            newColor = cellSettings.cellColors.Colors[cellSettings.cellColors.ColorCount - cellModel.DigDeep];
        }
        cellView.SetCellColor(newColor);

        cellView.OnViewClick += CellView_OnViewClick;
    }

    public void PlaceView(Vector3 position)
    {
        cellView.PlaceView(position);
    }

    private void CellView_OnViewClick()
    {
        cellGameLogic.CheckLogic(cellModel,cellView);
        
    }

    

    public class CellControllerFactory : PlaceholderFactory<CellGameLogic.CellSettings, ICellController> { }
    public class CellModelFactory: PlaceholderFactory<CellGameLogic.CellSettings, ICellModel> { }
    public class CellViewFactory: PlaceholderFactory<ICellView> { }

    internal class CellGameLogicFactory : PlaceholderFactory<ICellGameLogic> { }


}
