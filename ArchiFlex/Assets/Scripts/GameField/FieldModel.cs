using UnityEngine;

public interface IFieldModel
{
    int GridSize { get; }
    int GridRows { get; }
    int GridColumns { get; }
    Rect FieledRect { get; }
    Rect CellRect { get; }
}


public class FieldModel:IFieldModel
{
    FieldModelSettings settings;
    public FieldModel(FieldModelSettings settings)
    {
        this.settings = settings;
        Initialize();
        
    }

    private void Initialize()
    {
        GridSize = settings.ColumnsCount * settings.RowsCount;
        FieledRect = settings.aviableFieldRect;
    }

    

    public int GridSize { get; set; }
    public Rect FieledRect { get; set; }

    public int GridRows => settings.RowsCount;

    public int GridColumns => settings.ColumnsCount;

    public Rect CellRect => settings.cellRect;

    [System.Serializable]
    public class FieldModelSettings
    {
        [SerializeField] internal FieldGenerator.Settings settings;
        [SerializeField] internal Rect cellRect;
        [SerializeField] internal Rect aviableFieldRect;

        internal int ColumnsCount => settings.columnsCount;
        internal int RowsCount => settings.rowsCount;
    }
}

