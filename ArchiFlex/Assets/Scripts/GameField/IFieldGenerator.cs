﻿using System.Collections.Generic;

internal interface IFieldGenerator : IController
{
    void GenerateField(FieldModel.FieldModelSettings fieldModelSetting);
    void RegenerateField();
    void LoadFieldFromSavedData(GameSaveData gameSaveData, FieldModel.FieldModelSettings fieldModelSettings);
    List<CellModel> CellModels();
}
