using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;


public class FieldView : MonoBehaviour, IFieldView
{
    [SerializeField] private Sprite fieldImage;

    public GameObject GameObject => gameObject;

    public void PlaceView(Vector3 position)
    {
        transform.position = position;
    }


    [Inject] 
    internal void Construct()
    {
          
    }

    
}
