using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class FieldGenerator : IFieldGenerator
{
    private IFieldModel fieldModel;
    private IFieldView fieldView;
    private ICellsGenerator cellGenerator;
    

    [Inject]
    FieldViewFactory viewFactory;
    [Inject]
    FieldModelFactory modelFactory;
    FieldGenerator.Settings fieldSettings;
    [Inject]
    CellGeneratorFactory cellGeneratorFactory;


    internal FieldGenerator( FieldGenerator.Settings settings)
    {
        fieldSettings = settings;
    }


    internal class FieldViewFactory : PlaceholderFactory<IFieldView> { }
    internal class FieldModelFactory : PlaceholderFactory<FieldModel.FieldModelSettings, IFieldModel> { }
    internal class CellGeneratorFactory : PlaceholderFactory< IFieldModel, ICellsGenerator> { }  



    

    public void GenerateField(FieldModel.FieldModelSettings fieldModelSettings)
    {
        fieldModelSettings.settings = fieldSettings;
        CreateField(fieldModelSettings);
        
        cellGenerator.GenerateNewCells();
    }

    private void CreateField(FieldModel.FieldModelSettings fieldModelSettings)
    {
        fieldModelSettings.settings = fieldSettings;
        fieldModel = modelFactory.Create(fieldModelSettings);
        fieldView = viewFactory.Create();

        var centerScreenPoint = new Vector3(fieldModel.FieledRect.width / 2, fieldModel.FieledRect.height / 2, Camera.main.nearClipPlane);
        var centerWorldPoint = Camera.main.ScreenToWorldPoint(centerScreenPoint);
        fieldView.PlaceView(centerWorldPoint);
        cellGenerator = cellGeneratorFactory.Create(fieldModel);
    }

    public List<CellModel> CellModels()
    {
        return cellGenerator.GetCellModels();
    }

    public void RegenerateField()
    {
        cellGenerator.Regenerate();
    }

    public void LoadFieldFromSavedData(GameSaveData gameSaveData, FieldModel.FieldModelSettings fieldModelSettings)
    {
        CreateField(fieldModelSettings);
        cellGenerator.LoadCellsFromSave(gameSaveData);
        
    }

    [System.Serializable]
    public class Settings
    {
        [Range(2,10)]
        [SerializeField]
        internal int rowsCount;
        [Range(2, 10)]
        [SerializeField]
        internal int columnsCount;
        [Range(1, 100)]
        [SerializeField]
        internal int space;
        
    }

}
